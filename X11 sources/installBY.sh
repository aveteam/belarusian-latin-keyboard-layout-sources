#!/bin/bash

#!/bin/bash

# Шлях до файлу /usr/share/X11/xkb/symbols/by
symbols_file="/usr/share/X11/xkb/symbols/by"

# Ім'я розкладки, яку потрібно видалити
layout_name="lacinka"

# Пошук початку та кінця блоку розкладки за ім'ям
start_line=$(grep -n "xkb_symbols \"$layout_name\" {" "$symbols_file" | cut -d: -f1)
if [ -n "$start_line" ]; then
    end_line=$(grep -n "^};[[:space:]]*$" "$symbols_file" | awk -F: -v start_line="$start_line" '$1 > start_line {print $1; exit}')
    start_line=$((start_line - 2))

    if [ $start_line -ge 0 ] && [ -n "$end_line" ]; then
        # Видаляємо блок розкладки з файлу
        sudo sed -i "${start_line},${end_line}d" "$symbols_file"
        echo "Błok raskładki \"$layout_name\" byŭ vydaleny z fajła $symbols_file."
        xml_file="/usr/share/X11/xkb/rules/evdev.xml"
        sudo xmlstarlet ed --inplace -d "/xkbConfigRegistry/layoutList/layout[configItem/name='by']/variantList/variant[configItem/name='lacinka']" "$xml_file"
    fi
else
    echo "Raskładka \"$layout_name\" raniej nie ŭstaloŭvalasia."
fi

# Шлях до XML-файлу
xml_file="/usr/share/X11/xkb/rules/evdev.xml"

# Перевірка наявності утиліти xmlstarlet та встановлення, якщо не встановлено
if ! command -v xmlstarlet &>/dev/null; then
    echo "xmlstarlet nie ŭstalavany. My ŭstaloŭvajem..."
    sudo apt-get update
    sudo apt-get install -y xmlstarlet
else
    echo "xmlstarlet dastupny, mahu praciahnuć ustalavańnie."
fi

# Додаємо новий variant до layout з іменем "by"
sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout[configItem/name='by']/variantList" -t elem -n variant_tmp -v "" $xml_file
sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout[configItem/name='by']/variantList/variant_tmp" -t elem -n configItem -v "" $xml_file
sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout[configItem/name='by']/variantList/variant_tmp/configItem" -t elem -n name -v "lacinka" $xml_file
sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout[configItem/name='by']/variantList/variant_tmp/configItem" -t elem -n description -v "Belarusian łacinka" $xml_file
sudo xmlstarlet ed --inplace -r "/xkbConfigRegistry/layoutList/layout[configItem/name='by']/variantList/variant_tmp" -v "variant" $xml_file

echo "Uniesieny źmieny ŭ $xml_file."

# Редагуємо файл /usr/share/X11/xkb/symbols/by
sudo tee -a /usr/share/X11/xkb/symbols/by << EOF

partial alphanumeric_keys
xkb_symbols "lacinka" {
        include "by(basic)"
        name[Group1]= "Belarusian łacinka";

        key <TLDE> {        [   dead_acute,  dead_acute,       none,        grave ]       };
        key <AE02> {        [            2,    quotedbl,       none,           at ]       };
        key <AE03> {        [            3,          No,       none,   numbersign ]       };
        key <AE04> {        [            4,   semicolon,     dollar,     EuroSign ]       };
        key <AE08> {        [            8,    asterisk,       none,         none ]       };
        key <AE11> {        [        minus,  underscore,     endash,       emdash ]       };

        key <AD01> {        [            q,           Q                           ]       };
        key <AD02> {        [            w,           W                           ]       };
        key <AD03> {        [            e,           E                           ]       };
        key <AD04> {        [            r,           R                           ]       };
        key <AD05> {        [            t,           T                           ]       };
        key <AD06> {        [            y,           Y                           ]       };
        key <AD07> {        [            u,           U                           ]       };
        key <AD08> {        [            i,           I                           ]       };
        key <AD09> {        [            o,           O                           ]       };
        key <AD10> {        [            p,           P                           ]       };
        key <AD11> {        [  bracketleft,   braceleft                           ]       };
        key <AD12> {        [ bracketright,  braceright                           ]       };

        key <AC01> {        [            a,           A                           ]       };
        key <AC02> {        [            s,           S,      sacute,      Sacute ]       };
        key <AC03> {        [            d,           D                           ]       };
        key <AC04> {        [            f,           F                           ]       };
        key <AC05> {        [            g,           G                           ]       };
        key <AC06> {        [            h,           H                           ]       };
        key <AC07> {        [            j,           J                           ]       };
        key <AC08> {        [            k,           K                           ]       };
        key <AC09> {        [            l,           L,     lstroke,     Lstroke ]       };
        key <AC10> {        [       zcaron,      Zcaron                           ]       };
        key <AC11> {        [       ubreve,      Ubreve,       U2019,  apostrophe ]       };
        key <BKSL> {        [    backslash,         bar,        slash             ]       };

        key <LSGT> {        [    backslash,       slash,         bar,         bar ]       };
        key <AB01> {        [            z,           Z,      zacute,      Zacute ]       };
        key <AB02> {        [            x,           X                           ]       };
        key <AB03> {        [            c,           C,      cacute,      Cacute ]       };
        key <AB04> {        [            v,           V                           ]       };
        key <AB05> {        [            b,           B                           ]       };
        key <AB06> {        [            n,           N,      nacute,      Nacute ]       };
        key <AB07> {        [            m,           M                           ]       };
        key <AB08> {        [       scaron,      Scaron                           ]       };
        key <AB09> {        [       ccaron,      Ccaron                           ]       };
        key <AB10> {        [       period,       comma,        less,     greater ]       };
};
EOF

echo "Uniesieny źmieny ŭ /usr/share/X11/xkb/symbols/by."

# Виконуємо команду для оновлення налаштувань клавіатури
sudo dpkg-reconfigure xkb-data

echo "Nieabchodna pierazapuścić X11, kab źmieny ŭstupili ŭ siłu."

# Перезавантаження X11
echo "Pačatak pracedury pierazahruzki X11..."
if command -v systemctl &>/dev/null; then
    sudo systemctl restart display-manager
else
    display_manager=$(cat /etc/X11/default-display-manager)
    case "$display_manager" in
        */lightdm) sudo systemctl restart lightdm ;;
        */gdm*) sudo systemctl restart gdm ;;
        */kdm) sudo systemctl restart kdm ;;
        */mdm) sudo systemctl restart mdm ;;
        *)
            echo "Nie atrymałasia vyznačyć adpaviedny dyśpietčar voknaŭ. Rablu poŭny pierazapusk systemy..."
            sudo reboot
            ;;
    esac
fi
echo "Praces pierazahruzki X11 zavieršany."
