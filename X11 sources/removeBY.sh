#!/bin/bash

# Шлях до файлу /usr/share/X11/xkb/symbols/by
symbols_file="/usr/share/X11/xkb/symbols/by"

# Ім'я розкладки, яку потрібно видалити
layout_name="lacinka"

# Пошук початку та кінця блоку розкладки за ім'ям
start_line=$(grep -n "xkb_symbols \"$layout_name\" {" "$symbols_file" | cut -d: -f1)
if [ -n "$start_line" ]; then
    end_line=$(grep -n "^};[[:space:]]*$" "$symbols_file" | awk -F: -v start_line="$start_line" '$1 > start_line {print $1; exit}')
    start_line=$((start_line - 2))

    if [ $start_line -ge 0 ] && [ -n "$end_line" ]; then
        # Видаляємо блок розкладки з файлу
        sudo sed -i "${start_line},${end_line}d" "$symbols_file"
        echo "Błok raskładki \"$layout_name\" byŭ vydaleny z fajła $symbols_file."
        xml_file="/usr/share/X11/xkb/rules/evdev.xml"
        sudo xmlstarlet ed --inplace -d "/xkbConfigRegistry/layoutList/layout[configItem/name='by']/variantList/variant[configItem/name='lacinka']" "$xml_file"
    fi
else
    echo "Raskładka \"$layout_name\" raniej nie ŭstaloŭvalasia."
fi

# Виконуємо команду для оновлення налаштувань клавіатури
sudo dpkg-reconfigure xkb-data

echo "Nieabchodna pierazapuścić X11, kab źmieny ŭstupili ŭ siłu."

# Перезавантаження X11
echo "Pačatak pracedury pierazahruzki X11..."
if command -v systemctl &>/dev/null; then
    sudo systemctl restart display-manager
else
    display_manager=$(cat /etc/X11/default-display-manager)
    case "$display_manager" in
        */lightdm) sudo systemctl restart lightdm ;;
        */gdm*) sudo systemctl restart gdm ;;
        */kdm) sudo systemctl restart kdm ;;
        */mdm) sudo systemctl restart mdm ;;
        *)
            echo "Nie atrymałasia vyznačyć adpaviedny dyśpietčar voknaŭ. Rablu poŭny pierazapusk systemy..."
            sudo reboot
            ;;
    esac
fi
echo "Praces pierazahruzki X11 zavieršany."
